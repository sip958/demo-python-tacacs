#!/bin/sh

TAC_PLUS_BIN=/tacacs/sbin/tac_plus
CONF_FILE=/etc/tacacs/tac_plus.conf

# Check configuration file exists
if [ ! -f ${CONF_FILE} ]; then
    echo "No configuration file at ${CONF_FILE}"
    exit 1
fi

# Check configuration file for syntax errors
${TAC_PLUS_BIN} -P -C ${CONF_FILE}
if [ $? -ne 0 ]; then
    echo "Invalid configuration file"
    exit 1
fi

echo "Starting server..."

# Start the server
exec ${TAC_PLUS_BIN} -G -C ${CONF_FILE} -d 8 -d 16 -d 64 -l /var/log/tac_plus/tac_plus.log